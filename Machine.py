from mimetypes import init
from flask import Flask, request, send_file
from numpy import average
import requests, time, threading, sys
from threading import Thread, Semaphore, Lock
import copy, random
from gevent import monkey
import operator
import hashlib, numpy

from gevent.pywsgi import WSGIServer

from Node import Node
from Job import Job
from RoundEntity import RoundEntity
import MyUtils

monkey.patch_all()


##########################
if(len(sys.argv) >= 2 and sys.argv[1] == "config_local"):
    # Remove the comment to use a topology with multi-terminal approach

    # 2 Nodi

    # GRAPH = { "Nodo_A" : {"ip":"127.0.0.5:8081", "latency": 0.0, "neighbors": ["Nodo_B"], "step_delta": 0.01, "migration": {}, "ratios_total": 0.0, "hash_effort": 5000 },
    #           "Nodo_B" : {"ip":"127.0.0.6:8081", "latency": 0.0, "neighbors": ["Nodo_A"], "step_delta": 0.01, "migration": {}, "ratios_total": 0.0, "hash_effort": 5000 }
    #         }

    # 3 Nodi

    GRAPH = { "Nodo_A" : {"ip":"127.0.0.5:8081", "latency": 0.0, "neighbors": ["Nodo_B", "Nodo_C"], "step_delta": 0.01, "migration": {}, "ratios_total": 0.0, "hash_effort": 500000  },
              "Nodo_B" : {"ip":"127.0.0.6:8081", "latency": 0.0, "neighbors": ["Nodo_A", "Nodo_C"], "step_delta": 0.01, "migration": {}, "ratios_total": 0.0, "hash_effort": 500000  }, 
              "Nodo_C" : {"ip":"127.0.0.7:8081", "latency": 0.0, "neighbors": ["Nodo_A", "Nodo_B"], "step_delta": 0.01, "migration": {}, "ratios_total": 0.0, "hash_effort": 500000  }
            }

    # 5 Nodi fully connected

    # GRAPH = { "Nodo_A" : {"ip":"127.0.0.5:8081", "latency": 0.0, "neighbors": ["Nodo_B","Nodo_C","Nodo_D","Nodo_E"], "step_delta": 0.01, "migration": {}, "ratios_total": 0.0, "hash_effort": 5000  },
    #           "Nodo_B" : {"ip":"127.0.0.6:8081", "latency": 0.0, "neighbors": ["Nodo_A","Nodo_C","Nodo_D","Nodo_E"], "step_delta": 0.01, "migration": {}, "ratios_total": 0.0, "hash_effort": 5000  }, 
    #           "Nodo_C" : {"ip":"127.0.0.7:8081", "latency": 0.0, "neighbors": ["Nodo_A","Nodo_B","Nodo_D","Nodo_E"], "step_delta": 0.01, "migration": {}, "ratios_total": 0.0, "hash_effort": 5000  },
    #           "Nodo_D" : {"ip":"127.0.0.8:8081", "latency": 0.0, "neighbors": ["Nodo_A","Nodo_B","Nodo_C","Nodo_E"], "step_delta": 0.01, "migration": {}, "ratios_total": 0.0, "hash_effort": 5000  },
    #           "Nodo_E" : {"ip":"127.0.0.9:8081", "latency": 0.0, "neighbors": ["Nodo_A","Nodo_B","Nodo_C","Nodo_D"], "step_delta": 0.01, "migration": {}, "ratios_total": 0.0, "hash_effort": 5000  }

    #         }

    # Ring 5 nodi

    # GRAPH = { "Nodo_A" : {"ip":"127.0.0.5:8081", "latency": 0.0, "neighbors": ["Nodo_B", "Nodo_E"], "step_delta": 0.01, "migration": {}, "ratios_total": 0.0, "hash_effort": 5000  },
    #           "Nodo_B" : {"ip":"127.0.0.6:8081", "latency": 0.0, "neighbors": ["Nodo_A", "Nodo_C"], "step_delta": 0.01, "migration": {}, "ratios_total": 0.0, "hash_effort": 5000  }, 
    #           "Nodo_C" : {"ip":"127.0.0.7:8081", "latency": 0.0, "neighbors": ["Nodo_B", "Nodo_D"], "step_delta": 0.01, "migration": {}, "ratios_total": 0.0, "hash_effort": 5000  },
    #           "Nodo_D" : {"ip":"127.0.0.8:8081", "latency": 0.0, "neighbors": ["Nodo_C", "Nodo_E"], "step_delta": 0.01, "migration": {}, "ratios_total": 0.0, "hash_effort": 5000  },
    #           "Nodo_E" : {"ip":"127.0.0.9:8081", "latency": 0.0, "neighbors": ["Nodo_A", "Nodo_D"], "step_delta": 0.01, "migration": {}, "ratios_total": 0.0, "hash_effort": 5000  }

    #         }

    # 5 Nodi star


    # GRAPH = { "Nodo_A" : {"ip":"127.0.0.5:8081", "latency": 0.0, "neighbors": ["Nodo_B","Nodo_C","Nodo_D","Nodo_E"], "step_delta": 0.01, "migration": {}, "ratios_total": 0.0, "hash_effort": 5000 },
    #         "Nodo_B" : {"ip":"127.0.0.6:8081", "latency": 0.0, "neighbors": ["Nodo_A"], "step_delta": 0.01, "migration": {}, "ratios_total": 0.0, "hash_effort": 5000  }, 
    #         "Nodo_C" : {"ip":"127.0.0.7:8081", "latency": 0.0, "neighbors": ["Nodo_A"], "step_delta": 0.01, "migration": {}, "ratios_total": 0.0, "hash_effort": 5000  },
    #         "Nodo_D" : {"ip":"127.0.0.8:8081", "latency": 0.0, "neighbors": ["Nodo_A"], "step_delta": 0.01, "migration": {}, "ratios_total": 0.0, "hash_effort": 5000  },
    #         "Nodo_E" : {"ip":"127.0.0.9:8081", "latency": 0.0, "neighbors": ["Nodo_A"], "step_delta": 0.01, "migration": {}, "ratios_total": 0.0, "hash_effort": 5000  }

    #         }
    
    NODO_NAME = "Nodo_A"

    IP = GRAPH[NODO_NAME]["ip"][:-5]
    PORT = GRAPH[NODO_NAME]["ip"][-4:]
    WHOAMI = NODO_NAME
    hash_effort = GRAPH[NODO_NAME]["hash_effort"]
    step_delta = GRAPH[NODO_NAME]["step_delta"]

    neighbors_init = {}
    for n in GRAPH[WHOAMI]["neighbors"]:
        neighbors_init[n] = GRAPH[n]["ip"]

    nodeMySelf = Node(IP, PORT, WHOAMI, neighbors_init, hash_effort)		#create node objecst
else:
    NODO_NAME = ""
    IP = "0.0.0.0"
    PORT = 80
    WHOAMI = NODO_NAME
    hash_effort = 0
    step_delta = 0
    nodeMySelf = ""


###########################
configDone = False
loggerEnabled = True    # True if we want a log system
algoEnabled = True      # True if we want to call the migraton


max_job_in_queue = 4

service_queue = [] # coda di servizio, max 10 job
waiting_queue = [] # coda di attesa, può essere lunga all'infinito

service_semaphore = Semaphore(1) # 10 consumatori
waiting_semaphore = Semaphore(0) # n produttori

waiting_mutex = Semaphore(1) # mutua esclusione sulla waiting_queue
service_mutex = Semaphore(1) # mutua esclusione sulla service_queue

queue_history_time = []  
queue_history_serv = []
queue_history_wait = []

queue_mutex = Semaphore(1)

average_on_round = 5
refresh_rate = 30

lower_bound_zone = 0.95
upper_bound_zone = 1.05

len_squeue = []
len_wqueue = []
len_queuess = 0

#####################################

migration_ratios = {}

latency_history = []
latency_average_zone_history = []
migration_ratios_history = []


lat_av = 0.0

latency_array = []
tot_lat_array = 0.0
tot_job = 0

tot_job_forward = 0
tot_job_arrived_client = 0
tot_job_arrived_nodes = 0
tot_job_discard = 0

ratios_total = 0.0

check_counter = Lock()

######################################

app = Flask(__name__)

@app.route("/setConfigNode", methods = ['POST'])
def setConfigNode():
    global GRAPH, NODO_NAME, WHOAMI, hash_effort, step_delta, loggerEnabled, nodeMySelf, algoEnabled, configDone, max_job_in_queue, average_on_round, refresh_rate, lower_bound_zone, upper_bound_zone

    jsonAsInput = request.get_json()
    try:
        loggerEnabled = eval(jsonAsInput['loggerEnabled'])
        algoEnabled = eval(jsonAsInput['algoEnabled'])

        max_job_in_queue = jsonAsInput['max_job_in_queue']
        average_on_round = jsonAsInput['average_on_round']
        refresh_rate = jsonAsInput['refresh_rate']
        lower_bound_zone = jsonAsInput['lower_bound_zone']
        upper_bound_zone = jsonAsInput['upper_bound_zone']
        
        GRAPH = jsonAsInput['graph_input']
        NODO_NAME = jsonAsInput['NODO_NAME']
        WHOAMI = NODO_NAME
        hash_effort = GRAPH[NODO_NAME]["hash_effort"]
        step_delta = GRAPH[NODO_NAME]["step_delta"]
        IP = GRAPH[NODO_NAME]["ip"][:-5]
        PORT = GRAPH[NODO_NAME]["ip"][-4:]  

        neighbors_init = {}
        for n in GRAPH[WHOAMI]["neighbors"]:
            neighbors_init[n] = GRAPH[n]["ip"]
        
        nodeMySelf = Node(IP, PORT, WHOAMI, neighbors_init, hash_effort)		#create node objecst
 
    except Exception as e:
        print("!!! input bad !!!")
        print(e)
        return "Input bad"
    
    time.sleep(5)

    print("[RUNNING UPDATE method]")
    s = Thread(target = update)
    s.start()

    t = Thread(target = worker)
    t.start()

    configDone = True
    return "Config received!"

def worker():
    global waiting_semaphore, service_semaphore, waiting_mutex, service_mutex, service_queue, service_mutex
    while True:
        """Worker thread, esegue job quando sono disponibili"""
        #print("[WORKER] Waiting for jobs")
        MyUtils.printLog(loggerEnabled, "[WORKER] Waiting for jobs", WHOAMI)

        # aspetta che ci sia un job in attesa
        waiting_semaphore.acquire()  # -1 waiting_sem
        # aspetta che ci sia spazio nella coda di servizio
        service_semaphore.acquire()  #  -1 service_sem
        # richiedi mutua esclusività su coda di attesa
        waiting_mutex.acquire()

        # rimuovi job dalla coda di attesa
        job = waiting_queue.pop()

        # rilascia exclusività su coda di attesa
        waiting_mutex.release()

        # acquisisci esclusività su coda di servizio
        service_mutex.acquire()
        # aggiungi il job alla coda di servizio
        service_queue.append(job)
        # rilascia esclusività su coda di servizio
        service_mutex.release()

        # setta l'indice nella coda di servizio (forse non serve)
        job.set_index(len(service_queue) - 1)

        #print("[WORKER] Scheduling job #%d" % job.get_id())
        MyUtils.printLog(loggerEnabled, "[WORKER] Scheduling job #%d" % job.get_id(), WHOAMI)

        job_thread = Thread(target=execute_job, args=[job])
        job_thread.start()
        
def execute_job(job):
    global service_mutex, service_queue, service_semaphore, service_mutex
    """Thread che esegue un job e lo rimuove dalla coda di servizio quando ha terminato"""
    start_time = time.time()
    job.execute()
    #print("[WORKER] Job #%d done in %.2fs" %
    #      (job.get_id(), time.time() - start_time))
    MyUtils.printLog(loggerEnabled, "[WORKER] Job #%d done in %.2fs" % (job.get_id(), time.time() - start_time), WHOAMI)

    # richiedi esclusività su coda di servizio
    service_mutex.acquire()
    # rimuovi job dalla coda di servizio
    service_queue.remove(job)
    # aggiungi una risorsa alla coda di servizio, sbloccando il worker
    service_semaphore.release()
    # rilascia esclusività sulla coda di servizio
    service_mutex.release()

def enqueue_job(job):
    global waiting_mutex, waiting_queue, waiting_semaphore, waiting_mutex
    """Inserisce un job nella coda di attesa"""
    #print(f"[ENQUE JOB] len(service_queue)={len(service_queue)} len(waiting_queue)={len(waiting_queue)}")
    MyUtils.printLog(loggerEnabled, f"[ENQUE JOB] len(service_queue)={len(service_queue)} len(waiting_queue)={len(waiting_queue)}", WHOAMI)


    # acquisisci esclusività su coda di attesa
    waiting_mutex.acquire()

    # aggiungi all'inizio della coda di attesa un job
    waiting_queue.insert(0, job)
    # aggiungi una risorsa alla coda di attesa, sbloccando la prima parte del worker
    waiting_semaphore.release()

    # rilascia esclusività su coda di attesa
    waiting_mutex.release()

def _is_job_to_forward():
    global GRAPH

    random_pick = random.random()
    cumulative_ratio = 0.0
    for (node_as_key, ratio) in GRAPH[NODO_NAME]["migration"].items():
        cumulative_ratio += ratio
        if random_pick < cumulative_ratio and cumulative_ratio > 0.0:
            return node_as_key
    return -1

@app.route("/wardrop", methods = ['POST'])
def wardrop():
    global migration_ratios, tot_lat_array, tot_job, tot_job_forward, tot_job_arrived_client, algoEnabled, check_counter, tot_job_discard, GRAPH
    t0 = time.time()
    tot_job_arrived_client += 1
    forward_node = _is_job_to_forward()
    if(algoEnabled == True and forward_node != -1):
        tot_job_forward += 1
        requests.post('http://'+GRAPH[forward_node]["ip"]+ '/do_hash')
        return f"hash proveniente da {NODO_NAME} eseguito su {forward_node}"
    else:
        if(len(waiting_queue) < max_job_in_queue ):
            MyUtils.printLog(loggerEnabled, f"esegue {NODO_NAME} perchè gli altri non hanno potuto", WHOAMI)
            job = Job(nodeMySelf, nodeMySelf.numberOfJob, t0)
            enqueue_job(job)
            job.wait_for_completion()
            tempo_tot = float(time.time()-t0)
            print("tempo tot =",tempo_tot)
            check_counter.acquire()
            tot_lat_array += tempo_tot
            tot_job += 1
            check_counter.release()
            MyUtils.printLog(loggerEnabled, f"[do_hash on {NODO_NAME}] TEMPO TOTALE: {tempo_tot}", WHOAMI)
            return f"eseguito il wardrop"
        else:
            tot_job_discard += 1
            return f"Job discard"


@app.route("/amIAlive", methods = ['GET'])
def isAlive():
    global configDone
    return {"status": "I am alive", "configDone": configDone}


@app.route("/get_len_queues", methods = ['GET'])              
def get_len_queues():
    global service_queue, waiting_queue

    aux = len(service_queue)+len(waiting_queue)
    len_squeue.append(len(service_queue))
    len_wqueue.append(len(waiting_queue))
    return str(aux)

@app.route("/get_lat_av", methods = ['GET'])  
def get_lat_av():
    global lat_av
    return str(lat_av)

@app.route("/get_status_node", methods = ['GET'])  
def get_status_node():
    global lat_av, step_delta, migration_ratios, ratios_total, GRAPH, NODO_NAME
    ret = { "lat_av": GRAPH[NODO_NAME]["latency"], "step_delta": GRAPH[NODO_NAME]["step_delta"], "migration": GRAPH[NODO_NAME]["migration"], "ratios_total": GRAPH[NODO_NAME]["ratios_total"] }
    return ret

def get_average_latency_last_n_round(number_rounds):
    global latency_history

    average = 0.0
    if(len(latency_history) >= number_rounds):
        reducedList = latency_history[-number_rounds:] # I get the last number_round latency values.
        for i in range(len(reducedList)):
            average += reducedList[i]
        average /= number_rounds
    else:
        for i in range(len(latency_history)):
            average += latency_history[i]
        average /= len(latency_history)

    return str(average)

def delta():
    global step_delta
    return step_delta

def can_be_given(how_much):
    global ratios_total
    return round(ratios_total + how_much, 2) <= 1.0

def can_be_given_node(node, how_much):
    ratios_total_node = GRAPH[node]["ratios_total"] 
    return round(ratios_total_node + how_much, 2) <= 1.0

def can_be_subtract(how_much):
    global ratios_total
    return round(ratios_total - how_much, 2) > 0.0
        
def can_be_subtract_to_node(how_much, nodo_target):
    global migration_ratios
    return round(GRAPH[NODO_NAME]["migration"][nodo_target] - how_much, 2) > 0.0


def update():
    #[WARDROP1]questo metodo contiene quello che dovrà essere svolto ogni round
    global upper_bound_zone, lower_bound_zone, latency_average_zone_history, ratios_total,tot_lat_array,tot_job, lat_av, len_queuess, service_queue, waiting_queue, migration_ratios, step_delta, migration_ratios_history, latency_history 

    round_counter = 0
    #printLog("[UPDATE] Inizio i round")
    MyUtils.printLog(loggerEnabled, "[UPDATE] Inizio i round", WHOAMI)

    # Init mirgation ratio and mij
    for node_as_key in nodeMySelf.get_neighbors().keys():
        if(node_as_key not in migration_ratios.keys()):
            migration_ratios.update({node_as_key: 0 })

    allNodesAlive = True
    #inizia a far partie i round ogni tot secondi
    while True:
        start = time.time()
        while allNodesAlive:
            counterNodesAlive = 1  # Start from 1 because, myself 
            for node in GRAPH:
                try:
                    if(node != WHOAMI):
                        r=requests.get(f'http://'+GRAPH[node]["ip"]+'/amIAlive') 
                        auxJSON = r.json()
                        if(auxJSON["status"] == "I am alive" and auxJSON["configDone"] == False):
                            raise Exception()
                        #print(f"{node} " + str(r.text))
                        MyUtils.printLog(loggerEnabled, f"{node} " + str(r.text), WHOAMI)
                        counterNodesAlive += 1
                        if counterNodesAlive == len(GRAPH):
                            allNodesAlive = False
                            break
                except:
                    #print(f"Il nodo {node} è disconnesso")
                    MyUtils.printLog(loggerEnabled, f"Il nodo {node} è disconnesso", WHOAMI)

        round_counter += 1
        r = RoundEntity(GRAPH, round_counter, time.time())
        #print(r.show_details())
        #print(f"#################### Round #{round_counter} started ####################")
        MyUtils.printLog(loggerEnabled, f"#################### Round #{round_counter} started ####################", WHOAMI)
        
        #quello che devo fare ad ogni round
        len_queuess = float(len(service_queue)+len(waiting_queue))

        for node in GRAPH:
            if node == WHOAMI:
                #latency_history.append(GRAPH[node]["latency"])
                # lat_av = len_queuess
                # if(len(latency_history) >= average_on_round):
                #     lat_av = (sum(latency_history[-average_on_round:]))/(len(latency_history[-average_on_round:]))
                lat_av = 0.0
                if(len(latency_array) > 0):
                    lat_av = latency_array[-1]
                if(len(latency_array) >= average_on_round):
                    lat_av = (sum(latency_array[-average_on_round:]))/(len(latency_array[-average_on_round:]))
                #printLog("Lat_av "+str(lat_av))
                #print(latency_history[-average_on_round:])
                GRAPH[node]["latency"] = lat_av
                GRAPH[node]["migration"] = migration_ratios
                GRAPH[node]["step_delta"] = step_delta
                GRAPH[node]["ratios_total"] = ratios_total
                lat = GRAPH[node]["latency"]
                r.set_lat(node, lat)
            else: 
                #aux = requests.get('http://'+GRAPH[node]["ip"]+'/get_len_queues')
                aux = requests.get('http://'+GRAPH[node]["ip"]+'/get_status_node')
                auxJSON = aux.json() 
                GRAPH[node]["latency"] = float(auxJSON["lat_av"])
                GRAPH[node]["step_delta"] = auxJSON["step_delta"] 
                GRAPH[node]["migration"] = auxJSON["migration"]
                GRAPH[node]["ratios_total"] = auxJSON["ratios_total"]           
                lat = GRAPH[node]["latency"]
                r.set_lat(node, lat)
                #except:
                #    print(f"[UPDATE LOOP] {node} not connected")
                #    sys.exit(1)         
            # print("[ROUND] machine: " + node +"\t \t" +"ip: "+ GRAPH[node]["ip"] + "\t latency: " + str(GRAPH[node]["latency"])) 
        #print(r.show_details())

        #print(GRAPH)

        # Calcolo la media delle latenze.
        averageLatency = GRAPH[NODO_NAME]["latency"]
        for node_as_key in GRAPH:
            # Il nodo in esame è un mio vicino?
            if(node_as_key in nodeMySelf.get_neighbors().keys()):
                averageLatency += GRAPH[node_as_key]["latency"]
        averageLatency /= (len(nodeMySelf.get_neighbors().keys())+1)

        average_latency_low =  lower_bound_zone * averageLatency
        average_latency_high = upper_bound_zone * averageLatency

        latency_average_zone_history.append({"average":averageLatency,"LOW":average_latency_low,"HIGH":average_latency_high})

        ratios_total = 0.0
        for n in GRAPH[NODO_NAME]["migration"]:
            ratios_total += GRAPH[NODO_NAME]["migration"][n]

        if(GRAPH[NODO_NAME]["latency"] <= average_latency_low):   # SOTTO
            if(ratios_total > 0 ):
                for node_as_key in nodeMySelf.get_neighbors().keys():
                    if(can_be_subtract_to_node(step_delta, node_as_key ) and can_be_subtract(step_delta) ):
                        tmp = round(GRAPH[NODO_NAME]["migration"][node_as_key] - step_delta, 2)
                        migration_ratios.update({node_as_key: tmp })
                        tmp2 = round(ratios_total - step_delta, 2)
                        ratios_total = tmp2
        
        if GRAPH[NODO_NAME]["latency"]  >= average_latency_high:
            for node_as_key in nodeMySelf.get_neighbors().keys():
                if(GRAPH[node_as_key]["latency"] >= average_latency_high):
                    if(can_be_subtract_to_node(step_delta, node_as_key) and can_be_subtract(step_delta) ):
                        tmp = round(GRAPH[NODO_NAME]["migration"][node_as_key] - step_delta, 2)
                        migration_ratios.update({node_as_key: tmp })
                        tmp2 = round(ratios_total - step_delta, 2)
                        ratios_total = tmp2

                elif GRAPH[node_as_key]["latency"] <= average_latency_low:
                    if can_be_given(GRAPH[node_as_key]["step_delta"]):
                        tmp = round(GRAPH[NODO_NAME]["migration"][node_as_key] + GRAPH[node_as_key]["step_delta"], 2)
                        migration_ratios.update({node_as_key: tmp })
                        tmp2 = round(ratios_total + GRAPH[node_as_key]["step_delta"], 2)
                        ratios_total = tmp2


        #print(f"#################### Round #{round_counter} closed  in {time.time()-start} ####################\n")
        MyUtils.printLog(loggerEnabled, f"#################### Round #{round_counter} closed  in {time.time()-start} ####################\n", WHOAMI)
        
        if(tot_job == 0):
            latency_array.append(0)
        else:
            latency_array.append((tot_lat_array/tot_job))
        len_queuess = float(len(service_queue)+len(waiting_queue))
        latency_history.append(len_queuess)
        check_counter.acquire()
        tot_lat_array = 0
        tot_job = 0
        check_counter.release()
        time.sleep(refresh_rate)

        migration_ratios_history.append(copy.deepcopy(migration_ratios))

@app.route("/do_hash", methods=['GET','POST'])
def do_hash():
    global tot_lat_array, tot_job, tot_job_arrived_nodes, tot_job_discard
    
    if request.method == 'POST':
        if(len(waiting_queue) < max_job_in_queue):
            tot_job_arrived_nodes += 1
            t0 = time.time()
            job = Job(nodeMySelf, nodeMySelf.numberOfJob, t0)
            enqueue_job(job)
            job.wait_for_completion()
            tempo_tot = float(time.time()-t0)
            check_counter.acquire()
            tot_lat_array += tempo_tot
            tot_job += 1
            check_counter.release()
            #print(f"[do_hash on {WHOAMI}] TEMPO TOTALE: {tempo_tot}")
            MyUtils.printLog(loggerEnabled, f"[do_hash on {WHOAMI}] TEMPO TOTALE: {tempo_tot}", WHOAMI)
            return "do_hash executed on " + WHOAMI
        else:
            tot_job_discard += 1
            return f"eseguito il wardrop"

    if request.method == 'GET':
        nodeMySelf.do_hash(time.time())
        return "fatto"
@app.route("/performHashEffort", methods=["GET"])
def performHashEffort():

    try:
        data = request.get_json()
        iterations = data["iterations"]
        test = data["test"]
        mu = data["mu"]
        times = []
        for i in range(test):
            start = time.time()
            aux = []
            for _  in range(iterations):
                h = hashlib.sha512()
                p = "Tesi on fog computing"
                h.update(p.encode('utf-8'))
                aux.append(h.hexdigest())
            end = time.time()
            times.append(end-start)

        avg = numpy.average(numpy.array(times))
        var = numpy.var(numpy.array(times))

        oneIteration = avg/iterations
        hashEffort = (1/mu)/oneIteration
        ret = { "iterations": iterations, "average":avg, "variance": var, "mu": mu, "hashEffort for node": hashEffort }
    except:
        print("Malfomed request")
        ret = {"result":"error"}
    return ret

@app.route("/stats", methods=["GET"])
def stats():
    ret = ""
    ret += "Node: "+NODO_NAME+"\n"
    ret += "Number of job forward: "+str(tot_job_forward)+"\n"
    ret += "Number of job received from other node: "+str(tot_job_arrived_nodes)+"\n"
    ret += "Number of job received from client: "+str(tot_job_arrived_client)+"\n"
    ret += "Number of job discard: "+str(tot_job_discard)+"\n"
    ret += "Migration History: "+str(migration_ratios_history)+"\n"
    ret += "Latency History v2(time): "+str(latency_array)+"\n"
    ret += "Latency Zone: "+str(latency_average_zone_history)+"\n"
    return ret

# @app.route("/latencyHistory", methods=["GET"])
# def latencyPlot():
#     return MyUtils.doLatencyPlot(latency_array, NODO_NAME)

# @app.route("/migrationRatiosHistory", methods=["GET"])
# def migrationRatioPlot():
#     return MyUtils.doMigrationRatioPlot(migration_ratios_history, NODO_NAME)

# @app.route("/topology", methods=["GET"])
# def topology():
#     return MyUtils.doTopologyPlot(GRAPH)

@app.route("/getLatencyHistory", methods=["GET"])
def get_latency_history():
    return str(latency_array)


# @app.route("/summaryLatency", methods=["GET"])
# def summary_latency():
#     global latency_history
#     latency_all_nodes = { NODO_NAME: latency_array}

#     for node in GRAPH.keys():
#         if(node != NODO_NAME):
#             res = requests.get('http://'+GRAPH[node]["ip"]+'/getLatencyHistory')
#             latency_all_nodes.update({node: res.json()})
    
#     minLenList = len(latency_all_nodes[NODO_NAME])
#     for node in GRAPH.keys():
#         if(len(latency_all_nodes[node]) < minLenList ):
#             minLenList = len(latency_all_nodes[node])

#     listTruncated = []
#     for node in GRAPH.keys():
#         listTruncated = copy.deepcopy(latency_all_nodes[node][:minLenList])
#         latency_all_nodes[node] = copy.deepcopy(listTruncated)
#         listTruncated = [] 

#     return MyUtils.doLatencyMultiPlot(latency_all_nodes)

def printLog(text):
    print("[Node: "+NODO_NAME+" ]\t# "+text)


#########################
@app.route("/")
def hello_word():
    return f"It is running"
#########################


if __name__ == '__main__':

    if(len(sys.argv) >= 2 and sys.argv[1] == "config_local"):
        print("[RUNNING UPDATE method]")
        s = Thread(target = update)
        s.start()

        t = Thread(target=worker)
        t.start()
    else:
        print("---> Waiting config")

    # app.run(host=IP, port = PORT, debug=True, use_reloader=False)

    http_server = WSGIServer((IP, int(PORT)), app)
    http_server.serve_forever() 
