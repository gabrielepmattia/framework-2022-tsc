#UTILS
import requests, time, threading, os, io, base64
from flask import send_file
import numpy
import networkx as nx


PLOT_DIR = "./plots"

# POST
# def introduceMe(url, myobj):
#     x = requests.post(IP_SERVER+url, data = myobj)
#     return "Done"

# def doGet(url):
#     x = requests.get(IP_SERVER+url)
#     return x.text

def check_dir_exists_or_make(directory_name):
    parent_dir = "./"
    path = os.path.join(parent_dir, directory_name)
    if(os.path.exists(path)):
        return "Exists"
    else:
        os.mkdir(path)
        return "Make dir"

def mkdir_p(mypath):
    '''Creates a directory. equivalent to using mkdir -p on the command line'''

    from errno import EEXIST
    from os import makedirs,path

    try:
        makedirs(mypath)
    except OSError as exc: # Python >2.5
        if exc.errno == EEXIST and path.isdir(mypath):
            pass
        else: raise

# def doMigrationRatioPlot(migrationList, node_name):
#     to_plot_x = []
#     to_plot_y = []
#     plt.clf()

#     if(len(migrationList) > 0):
#         allNode = list(migrationList[0])
#         node_migration = {} # node => list_migration 
#         for node in allNode:
#             migration_node_target = []
#             for round in range(len(migrationList)):
#                 migration_node_target.append(migrationList[round][node])

#             node_migration[node] = migration_node_target

#         if(len(allNode) == 1):
#             nodoKey = allNode[0]
#             print("nodoKey",nodoKey)
#             to_plot_x = list(range(len(node_migration[nodoKey])))
#             to_plot_y = node_migration[nodoKey]

#             print("X")
#             print(to_plot_x)
#             print("Y")
#             print(to_plot_y)

#             plt.title("Migration ratios used by "+node_name)
#             plt.ylabel('Migration Ratios')
#             plt.xlabel('Round')
#             plt.plot(to_plot_x,to_plot_y )
#         else:
#             fig, axes = plt.subplots(len(allNode))
#             ax_id = 0
#             for node in allNode:
#                 to_plot_y = node_migration[node]
#                 to_plot_x = list(range(len(node_migration[node])))
#                 axes[ax_id].plot(to_plot_x, to_plot_y, label = node)
#                 axes[ax_id].legend()
#                 ax_id += 1
            
#             fig.suptitle("Migration ratios used by "+node_name)
#             fig.supylabel('Migration Ratios')
#             fig.supxlabel('Round')


#     my_stringIObytes = io.BytesIO()
#     plt.savefig(my_stringIObytes, format='png')
#     plt.close()
#     my_stringIObytes.seek(0)
#     return send_file(my_stringIObytes,download_name='summaryLatency.png', mimetype='image/png', as_attachment=True)

# def doLatencyPlot(latencyArray, node_name):
#     to_plot_x = []
#     to_plot_y = []
#     plt.clf()
    
#     for round_number in range(len(latencyArray)):
#         to_plot_x.append(round_number)
#         to_plot_y.append(latencyArray[round_number])
    
#     plt.title("Latency history of "+node_name)
#     plt.ylabel('Latency')
#     plt.xlabel('Round')
#     plt.plot(to_plot_x,to_plot_y)

#     # makeFile = "True"
#     # if(makeFile == "True"):
#     #     output_dir = PLOT_DIR+"/"+node_name
#     #     mkdir_p(output_dir)
#     #     plt.savefig('{}/latency.png'.format(output_dir))
#     #     return "Plot done! - "+node_name

#     # else:
#     my_stringIObytes = io.BytesIO()
#     plt.savefig(my_stringIObytes, format='png')
#     plt.close()
#     my_stringIObytes.seek(0)
#     return send_file(my_stringIObytes,download_name='latency.png', mimetype='image/png', as_attachment=True)
        

#     #threadPlot = threading.Thread(target = plt.show)
#     #threadPlot.start()
    
    
# def doLatencyMultiPlot(multiLatency):
#     to_plot_x = []
#     to_plot_y = []
#     plt.clf()

#     for node in multiLatency.keys():
#         to_plot_y = multiLatency[node]
#         to_plot_x = list(range(len(multiLatency[node])))
#         plt.plot(to_plot_x, to_plot_y, label = node)

#     plt.legend()
#     plt.title("Latency history")
#     plt.ylabel('Latency')
#     plt.xlabel('Round')

#     # makeFile = "True"
#     # if(makeFile == "True"):
#     #     output_dir = PLOT_DIR
#     #     mkdir_p(output_dir)
#     #     plt.savefig('{}/summaryLatency.png'.format(output_dir))
#     #     return "Done!"
#     # else:
#     my_stringIObytes = io.BytesIO()
#     plt.savefig(my_stringIObytes, format='png')
#     plt.close()
#     my_stringIObytes.seek(0)
#     return send_file(my_stringIObytes,download_name='summaryLatency.png', mimetype='image/png', as_attachment=True)
    

# def doTopologyPlot(graph):

#     G = nx.DiGraph()
#     for n in graph.keys():
#         tmp = n
#         G.add_node(tmp)
#         for n2 in graph[n]["neighbors"]:
#             G.add_edge(n,n2)

#     pos = nx.spring_layout(G)

#     nx.draw_networkx_nodes(G, pos,node_size=1800,node_color="#7ec18d")
#     nx.draw_networkx_labels(G, pos,font_size=9)
#     nx.draw_networkx_edges(G, pos, edge_color='black', arrows = True)

#     # makeFile = "True"
#     # if(makeFile == 'True'):
#     #     output_dir = PLOT_DIR
#     #     mkdir_p(output_dir)

#     #     plt.savefig("{}/{}.pdf".format(output_dir, "topology"))
#     #     plt.close()
#     #     return "Topology done!"
#     # else:
#     my_stringIObytes = io.BytesIO()
#     plt.savefig(my_stringIObytes, format='png')
#     plt.close()
#     my_stringIObytes.seek(0)
#     return send_file(my_stringIObytes,download_name='topology.png', mimetype='image/png', as_attachment=True)


# def save_file_on_disk(file_content, type, fileName):

#     output_dir = PLOT_DIR
#     mkdir_p(output_dir)

#     if(type == 'plot' or type == "text"): 
#         save_file = open(output_dir+"/"+fileName, "wb")
#         save_file.write(file_content.content)
#         save_file.close()
#     else:
#         pass
    
#     return "File Saved!"

def printLog(enabled, txt, nodo_name=""):
    if(enabled):
        if(nodo_name == ""):
            print(txt)
        else:
            print("[Node: "+nodo_name+" ]\t# "+txt)
    