from this import d
import time, datetime, hashlib, queue

class Node:
    def __init__(self, ip, port,description, neighbors, h):
        self.ip=ip
        self.port=port  
        self.description=description
        self.meantime_exec = 0
        self.numberOfJob = 0
        self.last_time_job = time.time()
        self.timer = 5
        self.totalJob = 0
        # self.history_of_execution_time = [0]
        # self.history_of_time = [self.secondi(time.time())] 
        self.history_of_execution_time = []
        self.history_of_time = [] 
        self.STATUS = 0     #0 libero, 1 occupato
        self.starting_time = time.time()    #reference time to collect data
        self.q = queue.Queue(10)
        self.h = h

        self.neighbors = neighbors # Dict {"Nodo_Name": "IP"}

    def get_neighbors(self):
        return self.neighbors

    def __repr__(self): 
        return self.description

    def get_descr(self):
        return self.description

    def get_last_time_job(self):
        return self.last_time_job   

    # def sec_to_msec(self, s):
    #     '''Convert a vector expressed in seconds returning a vector 
    #     expressed in milliseconds rounded to third decimal
    #     '''
    #     out = []
    #     for i in range(len(s)):
    #         aux = s[i]*10**3
    #         aux= round(aux,3)   #rounds to microseconds
    #         out.append(aux)
    #     return out    

    def info(self):
        return "IP:" + str(self.ip)+ '\n' "PORT:" + str(self.port)

    def get_numberOfJob(self):
        return str(self.numberOfJob)

    def get_meantime_exec(self):
        return str(self.meantime_exec)  

    def get_status(self):
        return str(self.STATUS)      

    # restituisce la stringa dell'ip del server
    def ip_str(self):
        ip = self.ip
        port = self.port
        s = str(ip[0])+'.'+str(ip[1])+'.'+str(ip[2])+'.'+str(ip[3])+':'+str(port)               
        return s

    def only_ip_str(self):
        ip = self.ip
        s = str(ip[0])+'.'+str(ip[1])+'.'+str(ip[2])+'.'+str(ip[3])             
        return s

    # def set_adj(self,dic):  
    #     self.neighbors=dic
    #     return self.neighbors

    def print_log(self):
        if len(self.history_of_time) != len(self.history_of_execution_time):
            return "vettori di dimensione diversa"
        else:    
            with open(f'Log_file_{self.description}.txt','w') as f:
                for i in range(len(self.history_of_time)):
                    f.write(str(self.history_of_time[i]) + " ")
                    f.write(str(round((self.history_of_execution_time[i]*10**3),3)) + "\n") 
        return f"log performed"               
    
    # def add_adj(self,dic2): 
    #     aux = self.neighbors
    #     aux2=dic2
    #     for d in dic2:
    #         aux[d] = aux2[d]
    #     self.neighbors = aux    
    #     return self.neighbors
    
    def check_time(self):
        '''If #timer passed restart 
        meantime and number of job
        '''
        t = self.last_time_job
        now = time.time()
        if int(now - t) >= self.timer:
            self.meantime_exec = 0 
            self.numberOfJob = 0

    def secondi(self, t):  
        '''Restituisce un intero che e' il time.time() 
        troncato alla quarta cifra prima della virgola 
        e alla quarta dopo. Quindi xxxx.yyyy
        '''
        t=str(t)
        aux=''
        for i in range(len(t)):
            if i >= 6 and i<=16:
                aux = aux+t[i]
        secondi = float(aux)    
        return secondi

    # def wait(self):
    #     time.sleep(1)
    #     self.q.get()
    #     print(f"i slept for 1 sec")


    def do_hash(self, t):
        #print("_________________Hash started_________________")
        self.STATUS = 1
        # start = datetime.datetime.now()
        aux =[]
        for _  in range(self.h):
            h = hashlib.sha512()                                             #computing hashing function
            p = "Tesi on fog computing"
            h.update(p.encode('utf-8'))
            aux.append(h.hexdigest())
        #print(f"\
        #Hash evaluated!")  
        # self.q.get()
        end = time.time()
        time_elapsed = end - t                    #from the job start to the end of job
        self.history_of_time.append(self.secondi(time.time()))
        self.history_of_execution_time.append(round(time_elapsed,6))
        #evaluation of mean
        m0 = self.meantime_exec
        count=self.numberOfJob
        self.meantime_exec = m0 + (time_elapsed - m0)/(count + 1)
        self.numberOfJob = count + 1
        self.totalJob += 1
        self.last_time_job = time.time()  
        
        #print(f"\
        #Execution time for last job: {str(time_elapsed)} \n\
        #Mean time in {self.description}: {str(self.meantime_exec)} \n\
        #Number of recent job (last {self.timer} seconds): {str(self.numberOfJob)} \n\
        #Total job: \t {str(self.totalJob)}")
        #print("_________________Hash ended_________________")
        return f"do_hash terminated on {self.description}"
