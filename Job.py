from threading import Thread, Semaphore
import Node


class Job():

    def __init__(self, node, _id=0, _t0 = 0, index=0):
        self._id = _id
        self._t0 = _t0
        self._starting_time = 0.0
        self._waiting_semaphore = Semaphore(0)
        self._index = index
        self._node = node

    def wait_for_completion(self):
        self._waiting_semaphore.acquire()

    def done(self):
        self._waiting_semaphore.release()

    def get_id(self):
        return self._id

    def set_index(self, i):
        self._index = i

    def execute(self):
        self._node.do_hash(self._t0)
        self.done()